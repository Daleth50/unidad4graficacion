package Unidad4;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GraphicsConfiguration;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import javax.media.j3d.*;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.vecmath.Color3f;
import javax.vecmath.Point3f;
import javax.vecmath.Vector3f;
import com.sun.j3d.utils.geometry.*;
import com.sun.j3d.utils.image.TextureLoader;
import com.sun.j3d.utils.universe.*;

public class ProyectoTexturasIluminacion extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5393970558418259823L;
	Canvas3D canvas;
	DirectionalLight dirLight;
	AmbientLight ambLight;
	PointLight lamp;
	BranchGroup root = new BranchGroup();
	JButton btn1 = new JButton("seleccion de color");
	JButton btn2 = new JButton("seleccionar Tipo de Luz");
	JButton btn3 = new JButton("Cambiar texturas");
	JColorChooser Selectorcolor = new JColorChooser();
	Color selectedColor;
	Appearance ap = new Appearance();
	Material material = new Material();
	Box S = new Box(.3f, .3f, .3f, Box.GENERATE_NORMALS | Box.GENERATE_TEXTURE_COORDS, ap);
	ImageComponent2D imageT;
	Texture2D texture;
	TextureAttributes textAttr;
	JFileChooser fc = new JFileChooser();
	String route;
	URL url;
	BranchGroup contBranch;
	SimpleUniverse su;
	int res = 1;
	String imgRoute1,imgRoute2,imgRoute3,imgRoute4,imgRoute5,imgRoute6;

	public ProyectoTexturasIluminacion(String r1, String r2, String r3,String r4,String r5,String r6) {
		super("Proyecto Java3D");
		setSize(800, 600);
		setLocationRelativeTo(this);
		imgRoute1 = r1;
		imgRoute2 = r2;
		imgRoute3 = r3;
		imgRoute4 = r4;
		imgRoute5 = r5;
		imgRoute6 = r6;
		GraphicsConfiguration cfg = SimpleUniverse.getPreferredConfiguration();
		canvas = new Canvas3D(cfg);
		add(canvas);
		contBranch = escena();
		contBranch.setCapability(BranchGroup.ALLOW_DETACH);
		contBranch.compile();
		su = new SimpleUniverse(canvas);
		su.getViewingPlatform().setNominalViewingTransform();
		su.addBranchGraph(contBranch);
		JPanel panel = new JPanel();
		panel.add(btn2);
		panel.add(btn3);

		add(btn1, BorderLayout.NORTH);
		add(panel, BorderLayout.SOUTH);

		btn1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				selectedColor = JColorChooser.showDialog(null, "Seleccione un Color", Color.WHITE);
				lamp.setColor(new Color3f(selectedColor));
				dirLight.setColor(new Color3f(selectedColor));
				ambLight.setColor(new Color3f(selectedColor));

			}
		});

		btn2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub

				cLuces obj = new cLuces(ProyectoTexturasIluminacion.this, true);
				res = obj.mostrar();
				if(res != -1) {
					lamp.setEnable(false);
					ambLight.setEnable(false);
					dirLight.setEnable(false);
				}

			}
		});
		
		btn3.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				new cTextura(imgRoute1,imgRoute2,imgRoute3,imgRoute4,imgRoute5,imgRoute6);
				dispose();
				
			}
		});

		canvas.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				if (e.getButton() == 3) {
					lamp.setEnable(false);
					ambLight.setEnable(false);
					dirLight.setEnable(false);
				}
				if (e.getButton() == 1) {
					float x = (float) e.getX() / 1000;
					float y = (float) e.getY() / 1000;
					if(res == 0) {
						ambLight.setEnable(true);
					}
					if(res == 1) {
						lamp.setPosition(x, y, x > .5 ? 0 : 1);
						lamp.setEnable(true);
					}
					if(res == 2) {
						dirLight.setDirection(x, y, 0);
						dirLight.setEnable(true);
					}
				}
			}
		});

		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	
	public ImageComponent2D cargarImagen(String r) {
		ImageComponent2D imagen = null;
		
			URL ruta = getClass().getResource(r);
			if(ruta != null) {
			TextureLoader cargador = new TextureLoader(ruta, this);
			imagen = cargador.getImage();
			} else {
				try {
					ruta = new File(r).toURI().toURL();
					TextureLoader cargador = new TextureLoader(ruta, this);
					imagen = cargador.getImage();
				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}		
				
		return imagen;
	}

	public void PonerLuzDireccional(BranchGroup r, Vector3f pos1) {
		dirLight = new DirectionalLight(false, new Color3f(new Color(254, 254, 254, 1)), pos1);
		BoundingSphere limite = new BoundingSphere(); //new Point3d(0,0,0),1
		dirLight.setInfluencingBounds(limite);
		dirLight.setCapability(DirectionalLight.ALLOW_STATE_WRITE);
		dirLight.setCapability(DirectionalLight.ALLOW_DIRECTION_WRITE);
		dirLight.setCapability(DirectionalLight.ALLOW_DIRECTION_READ);
		dirLight.setCapability(DirectionalLight.ALLOW_COLOR_WRITE);
		dirLight.setInfluencingBounds(limite);
		r.addChild(dirLight);
	}

	public void PonerLampara(Color3f color, Point3f posicion, BranchGroup r) {
		lamp = new PointLight(false, color, posicion, new Point3f(1, 0, 1));
		lamp.setCapability(PointLight.ALLOW_STATE_WRITE);
		lamp.setCapability(PointLight.ALLOW_POSITION_WRITE);
		lamp.setCapability(PointLight.ALLOW_POSITION_READ);
		lamp.setCapability(PointLight.ALLOW_COLOR_WRITE);
		BoundingSphere limite = new BoundingSphere(); // new Point3d(0,0,0),1
		lamp.setInfluencingBounds(limite);
		r.addChild(lamp);

	}

	public void PonerLuzAmbiental(BranchGroup r) {
		ambLight = new AmbientLight(false, new Color3f(Color.WHITE));
		ambLight.setCapability(AmbientLight.ALLOW_STATE_WRITE);
		ambLight.setCapability(AmbientLight.ALLOW_COLOR_WRITE);
		BoundingSphere limite = new BoundingSphere(); // new Point3d(0,0,0),1
		ambLight.setInfluencingBounds(limite);
		r.addChild(ambLight);
	}

	public void AplicarTextura(String ruta, int cara) {

		// crear una textura
		imageT = cargarImagen(ruta);
		texture = new Texture2D(Texture.BASE_LEVEL, Texture.RGB, imageT.getWidth(), imageT.getHeight());
		texture.setImage(0, imageT);
		texture.setEnable(true);
		textAttr = new TextureAttributes();
		textAttr.setTextureMode(TextureAttributes.MODULATE);
		// apariencia
		ap = new Appearance();
		material = new Material();
		ap.setMaterial(material);
		ap.setTexture(texture);
		ap.setTextureAttributes(textAttr);
		S.setCapability(Shape3D.ALLOW_APPEARANCE_OVERRIDE_WRITE);
		S.setCapability(Shape3D.ALLOW_APPEARANCE_WRITE);
		S.setCapability(Shape3D.ALLOW_APPEARANCE_READ);
		S.getShape(cara).setAppearance(ap);

	}

	public BranchGroup escena() {

		PonerLuzAmbiental(root);
		PonerLuzDireccional(root, new Vector3f(-1,0,0));

		PonerLampara(new Color3f(new Color(254, 254, 254, 1)), new Point3f(1, 1, 1), root);

		// poner fondo en la pantalla
		ImageComponent2D fondo = cargarImagen("/Unidad4/texturas/fondo.jpg");
		BoundingSphere limite = new BoundingSphere();
		Background B = new Background(fondo);
		B.setApplicationBounds(limite);
		root.addChild(B);

		// crear una textura
		imageT = cargarImagen("/Unidad4/texturas/perrito1.jpg");
		texture = new Texture2D(Texture.BASE_LEVEL, Texture.RGB, imageT.getWidth(), imageT.getHeight());
		texture.setImage(0, imageT);
		texture.setEnable(true);
		textAttr = new TextureAttributes();
		textAttr.setTextureMode(TextureAttributes.MODULATE);
		// apariencia

		ap.setMaterial(material);
		ap.setTexture(texture);
		ap.setTextureAttributes(textAttr);

		AplicarTextura(imgRoute1, Box.BACK);
		AplicarTextura(imgRoute2, Box.BOTTOM);
		AplicarTextura(imgRoute3, Box.FRONT);
		AplicarTextura(imgRoute4, Box.LEFT);
		AplicarTextura(imgRoute5, Box.RIGHT);
		AplicarTextura(imgRoute6, Box.TOP);

		// rotación automatica
		TransformGroup tgRotacion = new TransformGroup();
		tgRotacion.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
		Alpha tiempo = new Alpha(-1, 5000);
		RotationInterpolator rot = new RotationInterpolator(tiempo, tgRotacion);
		rot.setSchedulingBounds(limite);
		tgRotacion.addChild(S);

		root.addChild(rot);
		root.addChild(tgRotacion);
		return root;

	}

	public static void main(String[] args) {
		new ProyectoTexturasIluminacion("/Unidad4/texturas/XD.jpg", "/Unidad4/texturas/perrito2.jpeg","/Unidad4/texturas/Rocksy.jpg", "/Unidad4/texturas/perrito2.jpeg","/Unidad4/texturas/perrito1.jpg","/Unidad4/texturas/perrito1.jpg" );

	}

}
