
package Unidad4;

import java.awt.BorderLayout;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JProgressBar;

/**
 * @author daleth
 *
 */
public class Splash extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Thread t;
	JLabel splash;
	JProgressBar current;
	int num = 0;

	public Splash() {
		this.setSize(600, 425);
		this.setLocationRelativeTo(null);
		this.setLayout(new BorderLayout());
		this.setUndecorated(true);
		URL route = getClass().getResource("/Unidad4/texturas/loading.gif");
		splash = new JLabel(new ImageIcon(route));
		current = new JProgressBar(0, 2000);
		current.setValue(0);
		current.setStringPainted(true);
		//this.add(splash, BorderLayout.CENTER);
		this.add(current, BorderLayout.SOUTH);
	}

	public void iterate() {
		while (num < 2000) {
			current.setValue(num);
			try {
				Thread.sleep(70);
			} catch (InterruptedException e) {
			}
			num += 95;
		}
	}

	/*public static void main(String[] args) {
		new ProyectoTexturasIluminacion("/Unidad4/texturas/XD.jpg", "/Unidad4/texturas/perrito2.jpeg","/Unidad4/texturas/Rocksy.jpg", "/Unidad4/texturas/perrito2.jpeg","/Unidad4/texturas/perrito1.jpg","/Unidad4/texturas/perrito1.jpg" );
		Runnable ejecutable = new Runnable() {
			public void run() {
				Splash ventana = new Splash();
				ventana.setVisible(true);
				try {
					ventana.iterate();
					Thread.sleep(2 * 1000);

				} catch (Exception e) {

					e.printStackTrace();

				}

				ventana.dispose();

			}
		};

		Thread tarea = new Thread(ejecutable);
		tarea.start();
	}*/

}
