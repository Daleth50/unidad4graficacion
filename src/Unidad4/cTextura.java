package Unidad4;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;


public class cTextura extends JFrame {
	private static final long serialVersionUID = 7917227069231703359L;
	JLabel lbl1;
	JFileChooser fc=new JFileChooser();
	JButton btn1, btnAc, btnCan;
	JComboBox<String> combo;
	JList<String> jlist;
	DefaultListModel<String> list;
	String[] options = {"Trasera","Inferior","Frontal","Izquierda","Derecha","Superior"};
	String imgRoute1 = "/Unidad4/texturas/XD.jpg",
			imgRoute2 = "/Unidad4/texturas/perrito2.jpeg",
			imgRoute3 = "/Unidad4/texturas/Rocksy.jpg",
			imgRoute4 = "/Unidad4/texturas/perrito2.jpeg",
			imgRoute5 = "/Unidad4/texturas/perrito1.jpg",
			imgRoute6 = "/Unidad4/texturas/perrito1.jpg";

	public cTextura(String r1, String r2, String r3,String r4,String r5,String r6) {
		lbl1 = new JLabel("selecciona la cara");
		setTitle("Cambio de textura");
		setSize(300, 400);
		setResizable(false);
		setLocationRelativeTo(null);
		imgRoute1 = r1;
		imgRoute2 = r2;
		imgRoute3 = r3;
		imgRoute4 = r4;
		imgRoute5 = r5;
		imgRoute6 = r6;
		btn1 = new JButton("Selecciona la textura a aplicar");
		btnAc = new JButton("Aplicar");
		btnCan = new JButton("Default");
		combo = new JComboBox<String>(options);
		combo.setSelectedIndex(0);
		list = new DefaultListModel<String>();
		list.addElement(imgRoute1);
		list.addElement(imgRoute2);
		list.addElement(imgRoute3);
		list.addElement(imgRoute4);
		list.addElement(imgRoute5);
		list.addElement(imgRoute6);
		jlist = new JList<String>(list);
		JPanel comboPanel = new JPanel();
		comboPanel.add(lbl1);
		comboPanel.add(combo);
		JPanel buttons = new JPanel();
		buttons.add(btnAc);
		buttons.add(btnCan);
		JPanel botton = new JPanel(new GridLayout(2, 1));
		botton.add(btn1);
		botton.add(buttons);
		add(comboPanel, BorderLayout.NORTH);
		add(jlist,BorderLayout.CENTER);
		add(botton,BorderLayout.SOUTH);
		
		btn1.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				//Abrimos la ventana, guardamos la opcion seleccionada por el usuario
				int seleccion=fc.showOpenDialog(cTextura.this);
				if(seleccion==JFileChooser.APPROVE_OPTION){
				    File fichero=fc.getSelectedFile();
				    list.setElementAt(fichero.getAbsolutePath(), combo.getSelectedIndex());
				    
				}
				
			}
		});
		
		btnAc.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				Runnable ejecutable = new Runnable() {
					public void run() {
						Splash ventana = new Splash();
						ventana.setVisible(true);
						try {
							ventana.iterate();
							new ProyectoTexturasIluminacion(list.get(0), list.get(1), list.get(2), list.get(3), list.get(4), list.get(5));
							Thread.sleep(2 * 1000);
						} catch (Exception e) {
							e.printStackTrace();
						}
						
						ventana.dispose();

					}
				};
				
				Thread tarea = new Thread(ejecutable);
				tarea.start();
				dispose();
			}
		});
		
		btnCan.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				Runnable ejecutable = new Runnable() {
					public void run() {
						Splash ventana = new Splash();
						ventana.setVisible(true);
						try {
							ventana.iterate();
							new ProyectoTexturasIluminacion("/Unidad4/texturas/XD.jpg", "/Unidad4/texturas/perrito2.jpeg","/Unidad4/texturas/Rocksy.jpg", "/Unidad4/texturas/perrito2.jpeg","/Unidad4/texturas/perrito1.jpg","/Unidad4/texturas/perrito1.jpg" );
							Thread.sleep(2 * 1000);
						} catch (Exception e) {
							e.printStackTrace();
						}
						
						ventana.dispose();

					}
				};
				
				Thread tarea = new Thread(ejecutable);
				tarea.start();
				dispose();
				
			}
		});
		
		setVisible(true);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
	}
	

	/*public static void main(String[] args) {
		new cTextura();
	}*/

}
