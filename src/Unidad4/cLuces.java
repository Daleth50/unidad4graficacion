package Unidad4;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;


public class cLuces extends JDialog {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7542451524655179938L;
	JLabel etiq;
	JRadioButton ambient, point, directional;
	ButtonGroup gb;
	JButton Ac, Ca;
	int dev;

	public cLuces(JFrame V, boolean modal) {
		super(V, modal);
		setTitle("Reflección");
		setSize(500, 90);
		setLayout(new FlowLayout());
		setLocationRelativeTo(this);
		etiq = new JLabel("Selecciona el tipo de luz");
		ambient = new JRadioButton("AmbientLight", true);
		point = new JRadioButton("PointLight");
		directional = new JRadioButton("DirectionalLight");
		gb = new ButtonGroup();
		gb.add(ambient);
		gb.add(point);
		gb.add(directional);

		Ac = new JButton("Aceptar");
		Ca = new JButton("Cancelar");
		add(etiq);
		add(ambient);
		add(point);
		add(directional);
		add(Ac);
		add(Ca);
		Ac.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (ambient.isSelected()) {
					dev = 0;
				}
				if (point.isSelected()) {
					dev = 1;
				}
				if (directional.isSelected()) {
					dev = 2;
				}
				setVisible(false);
				dispose();
			}
		});

		Ca.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dev = -1;
				setVisible(false);
				dispose();
			}
		});
		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
	}

	public int mostrar() {
		setVisible(true);
		return dev;
	}
}
